var proxyquire =  require('proxyquire')
  , should = require('should')
  , path = require('path')
  , connectorlockPath = path.normalize(__dirname+'/connectorlock.js')

var pathStub = {
  normalize: function(str){
    return connectorlockPath;
  }
}

exports.connectorlock_local = proxyquire.noCallThru().load('../lib/connectorlock-usafm-auth', 
  { 
    'path': pathStub
  });

exports.proxyquire = proxyquire;
exports.should = should;