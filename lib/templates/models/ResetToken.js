/**
 * ResetToken
 *
 * @module      :: Model
 * @description :: Describes a users reset token\
 */

module.exports = {

  attributes: require('connectorlock').models.resetToken.attributes({
    
    /* e.g.
    nickname: 'string'
    */
    
  }),

  beforeCreate: require('connectorlock').models.resetToken.beforeCreate,
  afterCreate: require('connectorlock').models.resetToken.afterCreate
};
