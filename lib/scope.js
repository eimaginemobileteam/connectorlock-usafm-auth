'use strict';
var _ = require('lodash');
var authConfig = require('./connectorlock-usafm-auth').authConfig;

/**
 * TODO these can be refactored later
 * @type {Object}
 */

module.exports = function(Auth, engine){
  var def = Auth.definition;

  if(!_.isUndefined(def.email)){
    return generateScope('email', engine);
  }else if(typeof def.username !== 'undefined'){
    return generateScope('username', engine);
  }else{
    var error = new Error('Auth model must have either an email or username attribute');
    throw error;
  }
};

function generateScope(scopeKey, engine){
  return {
    type: scopeKey,
    engine: engine,
    getOrCreateUserAuthObject: function(attributes, req, cb){
      var attr = {password: attributes.password};
      attr[scopeKey] = attributes[scopeKey];

      var criteria = {};
      criteria[scopeKey] = attr[scopeKey];

      if(authConfig.createOnNotFound){
        this.engine.findOrCreateAuth(criteria, attr, cb);
      }else{
        this.engine.findAuth(criteria, cb);
      }
    },
    getUserAuthObject: function(attributes, req, cb){
      var attr = {password: attributes.password};
      attr[scopeKey] = attributes[scopeKey];

      var criteria = {};
      criteria[scopeKey] = attr[scopeKey];

      this.engine.findAuth(criteria, cb);
    },
    createUserAuthObject: function(attributes, req, cb){
      var attr = {password: attributes.password, firstName: attributes.firstName, lastName: attributes.lastName};

      // add user to attributes if it exists
      // otherwise, it will not be passed to the auth link and a dup user will be created
      // if the consuming application is creating the user record first
      if (attributes.user) {
        attr.user = attributes.user
      }

      attr[scopeKey] = attributes[scopeKey];

      var criteria = {};
      criteria[scopeKey] = attr[scopeKey];

      this.engine.findOrCreateAuth(criteria, attr, cb);
    }
  };
}
