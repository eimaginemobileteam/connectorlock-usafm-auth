'use strict';
var bcrypt = require('bcrypt');
const freeze = require('./freeze.js');

/**
 * Login action
 */
module.exports = function(req, res){

  var scope = require('../../scope')(connectorlock.Auth, connectorlock.engine);
  var params = req.params.all();

  if(typeof params[scope.type] === 'undefined' || typeof params.password === 'undefined'){
    connectorlock.cycle.loginFailure(req, res, null, {error: 'Invalid '+scope.type+' or password'});
  }else{
    var pass = params.password;
    scope.getOrCreateUserAuthObject(params, req, function(err, user){
      if (err) {
        if (err.code === 'E_VALIDATION') {
          return res.status(400).json(err);
        } else {
          return res.serverError(err);
        }
      }
      if (user) {
        if(bcrypt.compareSync(pass, user.auth.password)){
        // Checks for a frozen account
          if (user.auth.unfreezesAt !== null && user.auth.unfreezesAt > (new Date())) {
            //Fails as the account is frozen
            return res.forbidden('Your account is presently frozen')
          }

          return freeze.unfreezeAccount(user.email)
          .then(() => {
            connectorlock.cycle.loginSuccess(req, res, user);
          });
        }else{
          return freeze.addFailedLogin(user.email)
          .then(() => {
            connectorlock.cycle.loginFailure(req, res, user, {error: 'Invalid '+scope.type+' or password'});
          });
        }
      } else {
        //TODO redirect to register
        connectorlock.cycle.loginFailure(req, res, null, {error: 'user not found'});
      }
    });
  }
};
