'use strict';

const bannableLoginFailures = 10;
// The number of failures which will freeze an account
const freezeTimeInMinutes = 60;
const freezeWindowInMinutes = 60;

const addFailedLogin = passedEmail => {
  return Auth.findOne({ email: passedEmail })
  .then(matchedAuth => {
    if (matchedAuth) {
      let unfreezesAt = null;
      const failedLogins = matchedAuth.failedLogins.filter(failedLogin => {
        return ((new Date().getTime()) - failedLogin.getTime()) < (1000 * 60 * freezeWindowInMinutes);
      });
      failedLogins.push(new Date());
      if (failedLogins.length > (bannableLoginFailures - 1)) {
        // This is the final(10 by default) failure within a time period(default 60m)
        unfreezesAt = new Date(new Date().getTime() + (1000 * 60 * freezeTimeInMinutes));
      }
      return connectorlock.Auth.update(matchedAuth.id,
        {
          unfreezesAt,
          failedLogins
        })
        .then(() => {
          if (failedLogins.length > (bannableLoginFailures - 1)) {
            return 'Your account has been frozen';
          }
          return `Failed login.  You have ${bannableLoginFailures - failedLogins.length} remaining`;
        });
    }
    throw new Error('No auth found during freeze process');
  });
};

const unfreezeAccount = email => {
  return Auth.findOne({ email })
  .then(foundAuth => {
    return Auth.update(foundAuth.id, {
      unfreezesAt: null
    })
  })
  .catch(error => {
    sails.log.error(`Failed to unfreeze Auth object. Error: ${error}`);
    throw new Error(`Failed to unfreeze Auth object.`);
  });
};

module.exports = {
  addFailedLogin,
  unfreezeAccount
}
