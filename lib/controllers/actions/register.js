'use strict';
var bcrypt = require('bcrypt');

/**
 * Login action
 */
module.exports = function(req, res){

  var scope = require('../../scope')(connectorlock.Auth, connectorlock.engine);
  var params = req.params.all();

  if(typeof params[scope.type] === 'undefined' || typeof params.password === 'undefined'){
    connectorlock.cycle.registerFailure(req, res, null, {error: 'Invalid '+scope.type+' or password'});
  }else{
    var pass = params.password;
    scope.getUserAuthObject(params, req, function(err, user){
      if (err) {
        if (err.code === 'E_VALIDATION') {
          return res.status(400).json(err);
        } else {
          return res.serverError(err);
        }
      }
      if (user) {
        connectorlock.cycle.registerFailure(req, res, null, {error: 'User already exists.'});
      } else {
        scope.createUserAuthObject(params, req, function(err, user){
          if (err) {
            if (err.code === 'E_VALIDATION') {
              return res.status(400).json(err);
            } else {
              return res.serverError(err);
            }
          }
          if (user) {
            connectorlock.cycle.registerSuccess(req, res, user);
          }
        });
      }
    });
  }
};
