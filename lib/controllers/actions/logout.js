'use strict';

/**
 * Logout action
 */
module.exports = function (req, res){
  connectorlock.cycle.logout(req, res);
};